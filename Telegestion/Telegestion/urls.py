from django.contrib import admin
from django.urls import path,include
from Apps.PanelAdministracion import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('Apps.PanelAdministracion.urls'))
]
