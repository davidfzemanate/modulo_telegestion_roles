# myapp/urls.py
from django.urls import path, include
from .views import GroupCreateView,GroupListView,UserListView
from django.contrib.auth import views as auth_views
from . import views,utils

urlpatterns = [
    path('', include('django.contrib.auth.urls')),
    path('', views.index, name='index'),
    path('dashboard/', views.dashboard, name='dashboard'),
    path('registrar/', views.RegistrarUsu.as_view(), name='registrar'),
    path('iniciarSesion/', views.iniciarSesion, name='iniciarSesion'),
    path('cerrarSesion/', views.cerrarSesion, name='cerrarSesion'),
    path('check-session/', views.check_session, name='check_session'),
    path('crear-grupo/', GroupCreateView.as_view(), name='crear-grupo'),
    path('group_list/', GroupListView.as_view(), name='group_list'),
    path('eliminar_grupo/<int:group_id>/', views.eliminar_grupo, name='eliminar_grupo'),
    path('user_list/', UserListView.as_view(), name='user_list'),
    path('editar_usuario/<int:user_id>/', views.editar_usuario, name='editar_usuario'),
    path('eliminar_usuario/<int:user_id>/', views.eliminar_usuario, name='eliminar_usuario'),
    path('mapas/',views.mapas, name='mapas'),
    path('luminaria_view/', views.luminaria_view, name='luminaria_view'),

    path('event_status/<str:painting_code>/', views.obtener_event_status, name='obtener_event_up'),
    path('event_up/<str:painting_code>/', views.obtener_event_up, name='obtener_event_up'),


]