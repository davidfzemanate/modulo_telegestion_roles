from django.apps import AppConfig


class PaneladministracionConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'Apps.PanelAdministracion'
