from django.contrib.auth.decorators import user_passes_test
import requests
from django.shortcuts import render
# Funciones para verificar si el usuario pertenece a los grupos 'admin' y 'consulta'
def is_in_admin_group(user):
    return user.is_authenticated and user.groups.filter(name='Admin').exists()

admin_required = user_passes_test(is_in_admin_group)

def is_in_consulta_group(user):
    return user.is_authenticated and user.groups.filter(name='consulta').exists()

consulta_required = user_passes_test(is_in_consulta_group)



# Endpoint de autenticación
LOGIN_URL = 'https://telegestion.emcali.net.co/user/login'
LOGIN_CREDENTIALS = {
    "username": "dfzemanate",
    "password": "8sK8IJTdSoHuqK"
}
BASE_URL = 'https://telegestion.emcali.net.co/api/find'

# Función para autenticarte y obtener el token
def obtener_token():
    try:
        response = requests.post(LOGIN_URL, json=LOGIN_CREDENTIALS, verify=False)
        print(f"Status Code: {response.status_code}")  # Imprime el código de estado
        print(f"Response JSON: {response.json()}")  # Imprime el JSON de respuesta
        if response.status_code == 200:
            data = response.json().get('data', {})
            token = data.get('access_token')
            if token:
                print(f"Access Token: {token}")  
                return token
            else:
                raise Exception('Token no encontrado en la respuesta')
        else:
            raise Exception(f'Error en la autenticación: {response.status_code} - {response.text}')
    except Exception as e:
        raise Exception(f'Error al obtener el token: {str(e)}')



# Función para realizar peticiones GET a la API con el token
def realizar_peticion_get_status(id_luminaria, token):
    url = f"https://telegestion.emcali.net.co/api/find/event_status/{id_luminaria}/"
    token = obtener_token() 
    headers = {
        'x-access-token': token,  # Usa x-access-token en lugar de Authorization
        'Content-Type': 'application/json'
    }
    try:
        # Realiza la solicitud GET
        response = requests.get(url, headers=headers, verify=False)
        
        if response.status_code == 200:
            return response.json()  # Retorna los datos de la luminaria como JSON
        else:
            return {'error': f'Error al obtener la luminaria. Código de estado: {response.status_code}'}
    except requests.exceptions.RequestException as e:
        print(f"Error al realizar la petición GET: {e}")
        return {'error': 'No se pudo obtener la información de la luminaria'}
    
    
    
# Función para realizar peticiones GET a la API con el token
def realizar_peticion_get_event_up(id_luminaria, token):
    url = f"https://telegestion.emcali.net.co/api/find/event_up/{id_luminaria}/"
    token = obtener_token() 
    headers = {
        'x-access-token': token,  # Usa x-access-token en lugar de Authorization
        'Content-Type': 'application/json'
    }
    try:
        # Realiza la solicitud GET
        response = requests.get(url, headers=headers, verify=False)
        
        if response.status_code == 200:
            return response.json()  # Retorna los datos de la luminaria como JSON
        else:

            return {'error': f'Error al obtener la luminaria. Código de estado: {response.status_code}'}
    except requests.exceptions.RequestException as e:
        print(f"Error al realizar la petición GET: {e}")
        return {'error': 'No se pudo obtener la información de la luminaria'}

