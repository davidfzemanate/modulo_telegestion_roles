from django.db import models
from django.contrib.auth.models import Group
from django.contrib.auth.models import AbstractUser
from django.contrib.auth.models import Group,Permission


class Role(Group):
    class Meta:
        verbose_name = 'Role'
        verbose_name_plural = 'Roles'
        verbose_name_plural = 'Roles'
        

class ModeloUsuario(AbstractUser):
    grupo = models.ForeignKey(Group, on_delete=models.SET_NULL, null=True, blank=True) 
    groups = models.ManyToManyField(
        Group,
        related_name="custom_user_set",  # Añade un related_name único
        blank=True,
        help_text=('The groups this user belongs to. A user will get all permissions '
                'granted to each of their groups.'),
        verbose_name=('groups'),
    )
    user_permissions = models.ManyToManyField(
        Permission,
        related_name="custom_user_permissions_set",  # Añade un related_name único
        blank=True,
        help_text=('Specific permissions for this user.'),
        verbose_name=('user permissions'),
    )
    
    def is_in_group(self, group_name):
        return self.groups.filter(name=group_name).exists()


class User(models.Model):
    id = models.BigAutoField(primary_key=True)
    password = models.CharField(max_length=128, verbose_name='password')
    last_login = models.DateTimeField(blank=True, null=True, verbose_name='last login')
    is_superuser = models.BooleanField(default=False, verbose_name='superuser status')
    username = models.CharField(max_length=150, unique=True, verbose_name='username')
    first_name = models.CharField(max_length=150, blank=True, verbose_name='first name')
    last_name = models.CharField(max_length=150, blank=True, verbose_name='last name')
    email = models.EmailField(max_length=254, blank=True, verbose_name='email address')
    is_staff = models.BooleanField(default=False, verbose_name='staff status')
    is_active = models.BooleanField(default=True, verbose_name='active')
    date_joined = models.DateTimeField(auto_now_add=True, verbose_name='date joined')

    class Meta:
        db_table = 'auth_user'  # Nombre de la tabla en la base de datos


class UserGroups(models.Model):
    id = models.BigAutoField(primary_key=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    group = models.ForeignKey(Group, on_delete=models.CASCADE)

    class Meta:
        db_table = 'auth_user_groups'  # 