let map;
let markerCluster;
let markers = [];
let comunas_array = [];
let barrios_array = [];
let nodos_array = [];
let infoWindow = null;
const CENTER = { lat: 3.413568, lng: -76.519832 };

$(document).ready(() => {
  // document
  //   .getElementById("search_input")
  //   .addEventListener("keydown", function (evt) {
  //     if (
  //       (evt.which != 8 && evt.which != 0 && evt.which < 48) ||
  //       evt.which > 57
  //     ) {
  //       evt.preventDefault();
  //     }
  //   });
});

// Función callback después de cargar la librería de Google Maps
function initMap() {
  // Instancia el mapa
  map = new google.maps.Map(document.getElementById("map"), {
    center: CENTER,
    zoom: 12,
    clickableIcons: false,
    mapTypeControl: true,
    mapTypeId: "roadmap",
    streetViewControl: true,
    // disableDefaultUI: true,
    gestureHandling: "greedy",
    styles: [
      {
        featureType: "poi",
        stylers: [{ visibility: "off" }],
      },
    ],
  });

  infoWindow = new google.maps.InfoWindow({
    content: "",
  });




  let options = {
    imagePath: "images/m",
    minimumClusterSize: 5,
  };

  // Trae los polígonos de todas las comunas de la ciudad
  getComunas();

  // Instancia del markerClusterer agregando los marcadores al grupo
  markerCluster = new MarkerClusterer(map, [], options);

  // Listener que cierra el infowindow cuando se hace click en el mapa
  google.maps.event.addListener(map, "click", function () {
    infoWindow.close();
  });


 

}

// Función para obtener los datos de luminarias



// Función para crear el contenido del infowindow
function createNodeContent(jsonResponse, nodo) {
  const dataArray = jsonResponse.data;

  // Verificar que sea un array y que no esté vacío
  if (!Array.isArray(dataArray) || dataArray.length === 0) {
    return '<p>No hay datos disponibles.</p>';
  }

  const data = dataArray[0]; // Puedes cambiar esto según tus necesidades

  // HTML del InfoWindow con el modal incluido
  let html = `
    <div>
      <h4 style="text-align:center" >Nodo ${nodo.painting_code}</h4>
      <table border="1" width="*" style="border-collapse: collapse;" cellpadding="5">
        <tbody>
          <tr>
            <td>Device EUI</td>
            <td>${data.devEui}</td>
          </tr>
          <tr>
            <td>Fecha de creación</td>
            <td>${data.creation_date}</td>
          </tr>
          <tr>
            <td>Voltaje</td>
            <td>${data.voltage}</td>
          </tr>
            <td>Corriente</td>
            <td>${data.current}</td>
          <tr>
            <td>reactive_power</td>
            <td>${data.reactive_power}</td>
          </tr>
          <tr>
          <td>Estado</td>
            <td>${data.is_on ? 'Encendido' : 'Apagado' }</td>
              <script>
                var estado = data.is_on ? 'Encendido' : 'Apagado';
                console.log(estado);
                document.write(estado);
              </script>
            </td>
          </tr>
        </tbody>
      </table>
      
      <!-- Botón para abrir el modal -->
      <button type="button" onclick="showModal(${nodo.painting_code})" class="btn btn-primary">
        Ver más detalles
      </button>

      <!-- Modal con pestañas -->
      <div class="modal" id="nodeModal-${nodo.painting_code}" style="display:none;">
        <div class="modal-dialog">
          <div class="modal-content">
            <!-- Encabezado del modal -->
            <div class="modal-header">
              <h5 class="modal-title">Información del Nodo ${nodo.painting_code}</h5>
              <button type="button" class="close" onclick="closeModal(${nodo.painting_code})">&times;</button>
            </div>

            <!-- Cuerpo del modal con pestañas -->
            <div class="modal-body">
              <ul class="nav nav-tabs">
                <li class="nav-item">
                  <a class="nav-link active" data-toggle="tab" href="#tab1-${nodo.painting_code}">Event_up</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" data-toggle="tab" href="#tab2-${nodo.id}">event_join</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" data-toggle="tab" href="#tab3-${nodo.id}">event_log</a>
                </li>
              </ul>

              <div class="tab-content">

                <!-- Contenido de la pestaña 1 -->
                <div class="tab-pane container active" id="tab1-${nodo.painting_code}">
                  <div id="content-tab1-${nodo.painting_code}">Cargando datos...</div>
                </div>
                <!-- Contenido de la pestaña 2 -->
                <div class="tab-pane container fade" id="tab2-${nodo.id}">
                  <div id="content-tab2-${nodo.id}">Cargando datos...</div>
                </div>
                <!-- Contenido de la pestaña 3 -->
                <div class="tab-pane container fade" id="tab3-${nodo.id}">
                  <div id="content-tab3-${nodo.id}">Cargando datos...</div>
                </div>
              </div>
            </div>

            <!-- Footer del modal -->
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" onclick="closeModal(${nodo.id})">Cerrar</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  `;

  return html;
}

//manejo del modal 
function showModal(painting_code) {
  // Mostrar el modal
  document.getElementById(`nodeModal-${painting_code}`).style.display = 'block';

  // Cargar los datos de las pestañas cuando se abre el modal
  loadDataForTabs(painting_code);
}

function closeModal(painting_code) {
  // Ocultar el modal
  document.getElementById(`nodeModal-${painting_code}`).style.display = 'none';
}

function createNodeContentTab1(jsonResponse) {
  const dataArray = jsonResponse.data;

  // Verificar que sea un array y que no esté vacío
  if (!Array.isArray(dataArray) || dataArray.length === 0) {
    return '<p>No hay datos disponibles.</p>';
  }

  const data = dataArray[0]; // Puede

  // Comenzamos la tabla
  let html = `
    <table border="1" width="100%" style="border-collapse: collapse;" cellpadding="5">
      <thead>
        <tr>
          <th>Device EUI</th>
          <th>Fecha de creación</th>
          <th>Voltaje</th>
          <th>reactive_power</th>
        </tr>
      </thead>
      <tbody>
  `;

  // Iteramos sobre cada elemento en dataArray para crear filas
  dataArray.forEach(data => {
    html += `
      <tr>
        <td>${data.devEui}</td>
        <td>${data.creation_date}</td>
        <td>${data.voltage}</td>
        <td>${data.reactive_power}</td> 
      </tr>
    `;
  });

  // Cerramos la tabla
  html += `
      </tbody>
    </table>
  `;

  return html;
}


//llenar datos en modal
function loadDataForTabs(painting_code) {
  // Cargar datos para Tab 1
  fetch(`/event_up/${painting_code}`)
    .then(response => response.json())
    .then(jsonResponse  => {
    console.log(jsonResponse)
    //  const content = createNodeContentTab1(dataArray);
    //  document.getElementById(`content-tab1-${painting_code}`).innerHTML = content;
    //})
    //.catch(error => console.error('Error cargando datos para Tab 1:', error));

    infoWindow.setContent(createNodeContentTab1(jsonResponse)); // Pasa los datos obtenidos al modal
          infoWindow.open({
            shouldFocus: false,
            map,
          });
    })
}



// Mueve el marcador a la comuna solicitada
function moveToZone(id) {
  markerCluster.clearMarkers();
  clearMarkers();
  $("#search_input").val("");
  if (id == 0) {
    // Limpia los polígonos de las comunas
    clearZones(comunas_array);
    clearNeighborhoods();
    $("#barrios").empty();
    barrios_array = [];
    showAllZones(comunas_array);
    map.panTo(CENTER);
    map.setZoom(12);
  } else {
    $.ajax({
      type: "GET",
      dataType: "json",
      url: `https://apialumbrado.emcali.net.co/infrastructure/searchNodeComuna/?comuna=${id}`,
      beforeSend: () => {
        Swal.fire({
          title: "Cargando información",
          allowOutsideClick: false,
          didOpen: () => {
            Swal.showLoading();
          },
        });
      },
    })
      .done(function (response) {
        $.ajax({
          type: "GET",
          dataType: "json",
          url: `https://apialumbrado.emcali.net.co/infrastructure/searchDistrictsByComuna/${id}/`,
        })
          .done((barrios) => {
            // Limpia los polígonos de las comunas
            clearZones(comunas_array);
            clearNeighborhoods();
            Swal.close();
            map.setZoom(14);
            listNeighborhoods(barrios, "barrios");
            drawNeighborhoods(barrios);

            drawNodes(response.data);
            // Resaltar comuna activa
            showZone(comunas_array[id]);
            map.panTo(comunas_array[id].metadata.center);
            comunas_array[id].metadata.nodos = response.data;
          })
          .fail(function (error_obj) {
            errorMessage("La solicitud de barrios ha fallado");
          });
      })
      .fail(function (error_obj) {
        errorMessage("La solicitud de comunas ha fallado");
      });
  }
}

// Muestra una comuna/zona rural en el mapa
function showZone(zone) {
  zone.setOptions({ strokeColor: "#0000FF", fillColor: "#0000FF" });
  zone.setMap(map);
}

// Muestra todas las comunas y zonas rurales en el mapa
function showAllZones(zones) {
  zones.forEach((zone) => {
    zone.setOptions({ strokeColor: "#000000", fillColor: "#000000" });
    zone.setMap(map);
  });
}

// Elimina las comunas del mapa
function clearZones(zones) {
  zones.forEach((zone) => {
    zone.setMap(null);
  });
}

// Pinta los polígonos de los barrios en el mapa
function drawNeighborhoods(barrios) {
  clearNeighborhoods();
  barrios_array = [];
  barrios.forEach((barrio) => {
    barrios_array[barrio.pk] = new google.maps.Polygon({
      paths: barrio.polygon,
      strokeColor: "#0000FF",
      strokeOpacity: 0.8,
      strokeWeight: 2,
      fillColor: "#0000FF",
      fillOpacity: 0.1,
    });

    // Listener que cierra el infowindow cuando se hace click en el barrio
    google.maps.event.addListener(
      barrios_array[barrio.pk],
      "click",
      function () {
        infoWindow.close();
      }
    );

    barrios_array[barrio.pk].setMap(map);
  });
}

// Elimina los barrios del mapa
function clearNeighborhoods() {
  barrios_array.forEach((barrio) => {
    barrio.setMap(null);
  });
}

// Arregla el arreglo en orden alfabético
function orderArrayByString(arreglo) {
  let nuevo_arreglo = Array.from(arreglo).filter((item) => item != undefined);
  nuevo_arreglo.sort(function (a, b) {
    const nameA = a.name.toUpperCase(); // ignore upper and lowercase
    const nameB = b.name.toUpperCase(); // ignore upper and lowercase
    if (nameA > nameB) {
      return 1;
    }
    if (nameA < nameB) {
      return -1;
    }

    // names must be equal
    return 0;
  });
  return nuevo_arreglo;
}

// Lista los barrios
function listNeighborhoods(barrios, select_input) {
  $("#" + select_input).empty();
  $("#barrios").append(
    $("<option>", {
      value: 0,
      text: "",
    })
  );

  orderArrayByString(barrios).forEach((barrio) => {
    $("#barrios").append(
      $("<option>", {
        value: barrio.pk,
        text: barrio.name,
      })
    );
  });
}

// Pinta los nodos del cluster de marcadores
function drawNodes(nodes) {
  markerCluster.clearMarkers();
  markers = [];
  nodes.forEach((nodo) => {
    let marker = new google.maps.Marker({
      position: new google.maps.LatLng(Number(nodo.lat), Number(nodo.lng)),
      icon: "node.png",
      label: {
        text: "" + nodo.painting_code,
        color: "white",
        fontSize: "10px",
        fontWeight: "bold",
        className: "node-painting-code",
      },
    });
    markers.push(marker);
    // Agrega el listener al marcador para abrir el infowindow
    marker.addListener("click", () => {
      // Centra el mapa en el marcador
      map.panTo(marker.position);
      // Agrega el contenido al infowindow
      fetch(`/event_up/${nodo.painting_code}/`)
        .then(response => response.json())
        .then(jsonResponse => {
          // Agrega el contenido al infowindow
          infoWindow.setContent(createNodeContent(jsonResponse, nodo)); // Pasa los datos obtenidos al modal
          infoWindow.open({
            shouldFocus: false,
            anchor: marker,
            map,
          });
        })
        .catch(error => {
          console.error('Error fetching node event data:', error);
        });
    });
  });
  markerCluster.addMarkers(markers, true);
}
// Mueve la orientación del mapa para centrarlo en los poligunos de barrios
function moveToNeighborhood(id) {
  clearNeighborhoods();
  markerCluster.clearMarkers();
  $("#search_input").val("");
  if (id == 0) {
    barrios_array.forEach((barrio) => {
      barrio.setOptions({ strokeColor: "#0000FF", fillColor: "#0000FF" });
      barrio.setMap(map);
    });
    drawNodes(comunas_array[$("#zonas").val()].metadata.nodos);
    map.panTo(comunas_array[$("#zonas").val()].metadata.center);
    map.setZoom(14);
  } else {
    $.ajax({
      type: "GET",
      dataType: "json",
      url: `https://apialumbrado.emcali.net.co/infrastructure/nodesDistrict/?disrict=${id}`,
      beforeSend: () => {
        Swal.fire({
          title: "Cargando información",
          allowOutsideClick: false,
          didOpen: () => {
            Swal.showLoading();
          },
        });
      },
    })
      .done((response_nodes_by_district) => {
        Swal.close();
        drawNodes(response_nodes_by_district.data);
        // markerCluster.clearMarkers();
        // Request de nodos por barrio
        barrios_array[id].setOptions({
          strokeColor: "#FF0000",
          fillColor: "#FF0000",
        });
        barrios_array[id].setMap(map);
        centerPolygon(barrios_array[id]);
      })
      .fail(function (error_obj) {
        errorMessage("La solicitud de nodos por barrio ha fallado");
        console.log(error_obj);
      });
  }
}

// Centra el polígono
function centerPolygon(polygon) {
  let bounds = new google.maps.LatLngBounds();
  polygon.getPath().forEach(function (element) {
    bounds.extend(element);
  });
  map.fitBounds(bounds);
}

// Función para el mensaje de error
function errorMessage(error_text) {
  Swal.fire({
    icon: "error",
    title: error_text,
  });
}

// Limpia los marcadores simples del mapa
function clearMarkers() {
  markers.forEach((marker) => {
    marker.setMap(null);
  });
  markers = [];
}

// Busca el nodo en la DB utilizando el código de pintado
function searchByPaintingCode(painting_code) {
  // Valida que el código de pintado sea de 7 dígitos
  if (painting_code.length != 7 && painting_code != 0 && painting_code != 1) {
    errorMessage("El código de pintado debe ser de 7 dígitos");
  } else {
    $.ajax({
      type: "GET",
      dataType: "json",
      url: `https://apialumbrado.emcali.net.co/infrastructure/searchNodesByPaintingCode/${painting_code}/`,
      beforeSend: () => {
        Swal.fire({
          title: "Cargando información",
          allowOutsideClick: false,
          didOpen: () => {
            Swal.showLoading();
          },
        });
      },
    })
      .done(function (nodos) {
        // Limpia los polígonos de las comunas
        clearZones(comunas_array);
        clearNeighborhoods();
        markerCluster.clearMarkers();
        clearMarkers();
        $("#zonas").val(0);
        $("#barrios").empty();
        Swal.close();
        nodos.data.forEach((nodo) => {
          let marker = new google.maps.Marker({
            position: new google.maps.LatLng(
              Number(nodo.lat),
              Number(nodo.lng)
            ),
            icon: "node.png",
            label: {
              text: "" + nodo.painting_code,
              color: "white",
              fontSize: "10px",
              fontWeight: "bold",
              className: "node-painting-code",
            },
            map,
          });
          markers.push(marker);
        });
        if (nodos.length > 1) {
          map.setZoom(12);
          map.setCenter(CENTER);
        } else {
          map.setZoom(14);
          map.setCenter(
            new google.maps.LatLng(
              Number(nodos.data[0].lat),
              Number(nodos.data[0].lng)
            )
          );
        }
        Swal.close();
      })
      .fail(function (error_obj) {
        errorMessage("La solicitud de buscar nodo ha fallado");
      });
  }
}

// Trae las comunas desde el backend y las guarda en el localStorage para futuras consultas
function getComunas() {
  if (localStorage.getItem("comunas") !== null) {
    // El elemento existe en localStorage
    data = JSON.parse(localStorage.getItem("comunas"));
    renderComunas(data);
  } else {
    $.ajax({
      type: "GET",
      dataType: "json",
      url: "https://apialumbrado.emcali.net.co/infrastructure/searchComuna/",
      beforeSend: () => {
        Swal.fire({
          title: "Cargando información",
          allowOutsideClick: false,
          didOpen: () => {
            Swal.showLoading();
          },
        });
      },
    })
      .done((comunas) => {
        Swal.close();
        window.localStorage.setItem("comunas", JSON.stringify(comunas));
        renderComunas(comunas);
      })
      .fail(function (error_obj) {
        errorMessage("La solicitud de comunas ha fallado");
        console.log(error_obj);
      });
  }
}

// Pinta las comunas en el mapa
function renderComunas(comunas) {
  // Loop para recorrer el arreglo de zonas
  comunas.forEach((item) => {
    // Crea los poligonos
    comunas_array[item.pk] = new google.maps.Polygon({
      paths: item.polygon,
      strokeColor: "#000000",
      strokeOpacity: 0.8,
      strokeWeight: 2,
      fillColor: "#000000",
      fillOpacity: 0.1,
    });
    // Agrega un listener al poligono
    google.maps.event.addListener(comunas_array[item.pk], "click", () => {
      moveToZone(item.pk);
      $('#zonas option[value="' + item.pk + '"]').attr("selected", "selected");
    });
    // Agrega la metadata a los poligonos
    comunas_array[item.pk].metadata = {
      id: item.pk,
      name: item.name,
      color: "#FF0000",
      tipo: item.tipo,
      center: item.center,
    };

    // Añade el poligono al mapa
    comunas_array[item.pk].setMap(map);

    // Añade el item al listado de zonas
    $("#zonas").append(
      $("<option>", {
        value: item.pk,
        text: item.name,
      })
    );
  });
}
