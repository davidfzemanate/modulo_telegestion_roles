from django import forms
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.models import User
from django.contrib.auth.models import Group,Permission
from .models import ModeloUsuario


#creacion del formulario de inicio de sesion
class LoginForm(forms.Form):
    usuario= forms.CharField()
    contraseña = forms.CharField(widget=forms.PasswordInput)

#grupo de roles
class GroupForm(forms.ModelForm):
    permissions = forms.ModelMultipleChoiceField(
        queryset=Permission.objects.all(),
        widget=forms.CheckboxSelectMultiple,
        required=False,
        label='Permisos'
    )
    class Meta:
        model = Group
        fields = ['name','permissions']


#form para registrar usuarios
class CustomUserCreationForm(UserCreationForm):
    username = forms.CharField(
        max_length=150, 
        required=True,  
        help_text='Requerido. Ingrese un nombre de usuario válido.'
    )
    email = forms.EmailField(
        max_length=254, 
        required=True,  
        help_text='Requerido. Ingrese una dirección de correo electrónico válida.'
    )
    first_name = forms.CharField(
        max_length=30, 
        required=True, 
        help_text='Requerido. Ingrese su nombre.'
    )
    last_name = forms.CharField(
        max_length=30, 
        required=True,  
        help_text='Requerido. Ingrese su apellido.'
    )
    password1 = forms.CharField(
        label='Contraseña', 
        widget=forms.PasswordInput,
        required=True 
    )
    password2 = forms.CharField(
        label='Confirmar contraseña', 
        widget=forms.PasswordInput,
        required=True  
    )
    group = forms.ModelChoiceField(
        queryset=Group.objects.all(), 
        required=True, 
        help_text='Requerido. Seleccione un grupo.'
    )

    class Meta:
        model = ModeloUsuario
        fields = ('username', 'email', 'first_name', 'last_name', 'password1', 'password2','group')

    def save(self, commit=True):
        user = super().save(commit=False)

        if self.cleaned_data['group']:
            user.grupo_id=self.cleaned_data['group'].id

        if commit:
            user.save()

        return user
    



class CustomAuthenticationForm(AuthenticationForm):
    username = forms.CharField(label='Username', max_length=254)
    password = forms.CharField(label='Password', widget=forms.PasswordInput)