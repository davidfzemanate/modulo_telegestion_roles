from django.shortcuts import render,get_object_or_404
from django.contrib.auth import authenticate, login
from django.contrib.auth import logout
from django.shortcuts import redirect
from .forms import LoginForm
from django.views.decorators.csrf import csrf_exempt
from django.contrib import messages
from django.views.generic import CreateView
from django.views.decorators.cache import never_cache
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from .forms import GroupForm,Group,CustomUserCreationForm
from django.views.generic import ListView
from django.http import JsonResponse
from django.urls import reverse_lazy
from django.views import View
from django.http import HttpResponse
from .models import ModeloUsuario
from .utils import admin_required 
from rest_framework.permissions import IsAuthenticated
import requests
from .utils import realizar_peticion_get_status, realizar_peticion_get_event_up, obtener_token
import json



#Manejo de las sesiones

##validacion de los campos - creacion de la sesion
def iniciarSesion(request):
    if request.method == 'POST':
        username = request.POST.get('usuario')
        password = request.POST.get('contraseña')
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            messages.success(request, '¡Inicio de sesión exitoso!')
            return JsonResponse({'success': True})
        else:
            return JsonResponse({'success': False, 'message': 'Error: Usuario o contraseña incorrectos.'})
    else:
        return render(request, 'index.html')
    
#cerrar sesion y redirigir
@never_cache
def cerrarSesion(request):

    logout(request)
    
    messages.warning(request, 'Cerrando Sesión')

    response = redirect('/')
    
    # Añadir encabezados para deshabilitar el caché
    response['Cache-Control'] = 'no-store, no-cache, must-revalidate, max-age=0'
    response['Pragma'] = 'no-cache'
    response['Expires'] = '0'
    
    # Eliminar cookies de sesión
    response.delete_cookie('mi_SessionCookie')  # Reemplaza con el nombre de tu cookie de sesión si es necesario
    
    return response


##Validacion del estado de la sesion
@never_cache
def check_session(request):
    if request.user.is_authenticated:
        return JsonResponse({"session_expired": False})
    else:
        return JsonResponse({"session_expired": True})






#Manejo de usuarios

##Registrar usuario
@method_decorator(login_required, name='dispatch')
@method_decorator(admin_required, name='dispatch')
class RegistrarUsu(View): 
    
    
    def get_context_data(self, **kwargs):
        context = {
            'form_registroUsu': CustomUserCreationForm(),
            'form_crearGroups': GroupForm(),
            'is_admin': self.request.user.is_in_group('Admin'),
            'is_authenticated': self.request.user.is_authenticated
        }
        context.update(kwargs)
        return context
    
    form_class = CustomUserCreationForm
    def post(self, request):
        form = self.form_class(request.POST)
        if form.is_valid():
            user = form.save()
            username = form.cleaned_data.get('username')
            messages.success(request, f'Usuario {username} registrado correctamente.')   
            # Obtener el ID del grupo seleccionado del formulario
            group_id = form.cleaned_data['group'].id
            
            if group_id:
                # Obtener el objeto Group correspondiente al ID
                group = Group.objects.get(id=group_id)
                # Asignar el usuario al grupo
                group.custom_user_set.add(user)
            return redirect('user_list')
        else:
            for field, errors in form.errors.items():
                for error in errors:
                    messages.error(request, f'{field}: {error}')
        context = self.get_context_data(form=form, errors=form.errors)
        return render(request, 'error.html', context )

    
##listado de usuarios

@method_decorator(login_required, name='dispatch')
@method_decorator(admin_required, name='dispatch')
class UserListView(ListView):
    model = ModeloUsuario
    template_name = 'user_list.html'
    context_object_name = 'users'
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form_registroUsu'] = CustomUserCreationForm()
        context['form_crearGroups'] = GroupForm()
        context['is_admin'] = self.request.user.is_in_group('Admin')
        context['is_authenticated'] = self.request.user.is_authenticated
        return context

##Editar un usuario
@login_required
@admin_required
def editar_usuario(request, user_id):
    user = get_object_or_404(ModeloUsuario, pk=user_id)
   

    if request.method == 'POST':
        try:
            # Procesar el formulario de edición de usuario
            new_username = request.POST.get('username', user.username)
            
            # Verificar si el nuevo nombre de usuario ya existe en otro usuario
            if ModeloUsuario.objects.exclude(id=user_id).filter(username=new_username).exists():
                raise ValueError('El nombre de usuario ya está en uso.')

            user.username = new_username
            user.email = request.POST.get('email', user.email)
            user.first_name = request.POST.get('first_name', user.first_name)
            user.last_name = request.POST.get('last_name', user.last_name)
            user.save()

            # Devuelve los datos actualizados del usuario en formato JSON
            updated_user_data = {
                'id': user.id,
                'username': user.username,
                'email': user.email,
                'first_name': user.first_name,
                'last_name': user.last_name,
                'date_joined': user.date_joined.strftime('%d de %B de %Y a las %H:%M')  # Formatea la fecha como deseas
            }
            return JsonResponse({'success': True, 'updated_user_data': updated_user_data})
        except Exception as e:
            return JsonResponse({'success': False, 'error': str(e)})
    
    # Si el método HTTP es GET, renderizar el formulario de edición
    return render(request, 'editar_usuario.html', {'user': user})

##Elimina los usuarios
@csrf_exempt
@login_required
@admin_required
@never_cache
def eliminar_usuario(request, user_id):
        user = get_object_or_404(ModeloUsuario, id=user_id)
        user.delete()
        return JsonResponse({'success': True})






#Manejo de los grupos o Roles
##crear los grupos de permisos
@method_decorator(login_required, name='dispatch')
@method_decorator(admin_required, name='dispatch')
class GroupCreateView(CreateView):
    form_class = GroupForm
    success_url = reverse_lazy('dashboard')
    
    def form_valid(self, form):
        response = super().form_valid(form)
        # Obtiene los permisos seleccionados del formulario
        permissions = form.cleaned_data['permissions']

        # Asigna los permisos al grupo recién creado
        group = self.object
        for permission in permissions:
            group.permissions.add(permission)
        
        if self.request.headers.get('x-requested-with') == 'XMLHttpRequest':
            messages.success(self.request, '¡Se guardo!')
            return JsonResponse({'status': 'ok'})
        else:
            messages.success(self.request, '¡Se guardó!')
            return response
        
    def form_invalid(self, form):
        if self.request.headers.get('x-requested-with') == 'XMLHttpRequest':
            return JsonResponse({'status': 'error', 'errors': form.errors})
        else:
            return super().form_invalid(form)
    

##listado de grupos
@method_decorator(login_required, name='dispatch')
@method_decorator(admin_required, name='dispatch')

class GroupListView(ListView):
    model = Group
    template_name = 'group_list.html'
    context_object_name = 'groups'
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form_registroUsu'] = CustomUserCreationForm()
        context['form_crearGroups'] = GroupForm()
        context['is_admin'] = self.request.user.is_in_group('Admin')
        context['is_authenticated'] = self.request.user.is_authenticated
        return context
    
    def get_queryset(self):
        return Group.objects.all() 



@csrf_exempt
@login_required
@admin_required
@never_cache
def eliminar_grupo(request, group_id):
    if request.method == 'POST':
        group = get_object_or_404(Group, pk=group_id)
        group.delete()
        return JsonResponse({'success': True})
    else:
        return JsonResponse({'success': False, 'error': 'Método no permitido'}, status=405)
    





#redirigir al dashboard
@login_required
@never_cache
def dashboard(request): 
    form_registroUsu = CustomUserCreationForm()
    form_crearGroups = GroupForm()
    is_consulta = request.user.is_in_group('consulta')

    context={
        'form_registroUsu':form_registroUsu,
        'form_crearGroups':form_crearGroups,
        'is_admin': request.user.is_in_group('Admin'),
        'is_consulta': is_consulta,
        'is_operador': request.user.is_in_group('Operador'),
        'is_authenticated': request.user.is_authenticated,
    }
    return render(request, 'dashboard.html',context )

#index page por defecto
def index(request): 
    login = LoginForm()
    if 'cerrando_sesion' in request.session:
        messages.warning(request, '¡Cerrando Sesión!')
        del request.session['cerrando_sesion']
    return render(request, 'index.html',{'login':login})



#Vista del mapa
@csrf_exempt
@login_required
@never_cache
def mapas(request):
    google_maps_api_key = 'AIzaSyDpNUcNhqZnjirJ53TByxl6TJIjACDYXzE'
    form_registroUsu = CustomUserCreationForm()
    form_crearGroups = GroupForm()
    context = {
        'google_maps_api_key': google_maps_api_key,
        'form_registroUsu':form_registroUsu,
        'form_crearGroups':form_crearGroups,
        'is_admin': request.user.is_in_group('Admin'),
        'is_authenticated': request.user.is_authenticated,
    }
    return render(request, 'maps.html',context)






#consumir json -prueba
#elimina los usuarios- solo el admin

def luminaria_view(request):
    url = 'https://run.mocky.io/v3/5bcdabc6-9bc2-4494-b5aa-eb677de98d58'
    try:
        response = requests.get(url)
        response.raise_for_status()  # Verifica que la solicitud fue exitosa
        luminarias = response.json()
    except requests.exceptions.RequestException as e:
        return HttpResponse(f'Error al obtener los datos: {e}', status=500)

    
    return render(request, 'luminarias.html', {'luminarias': json.dumps(luminarias)})




#consumo de apis con datos de las luminarias
##consumo del endpoint de event_status
@csrf_exempt
@admin_required
def obtener_event_status(request,painting_code):
    token = obtener_token()  # Obtiene el token de autenticación
    resultado = realizar_peticion_get_status(painting_code, token)  # Pasa el painting_code como parámetro

    if 'error' in resultado:
        return JsonResponse({'error': resultado['error']}, status=400)
    else:
        return JsonResponse({'data': resultado['data']})

##Consumo del endpoint de event_up
@csrf_exempt
@admin_required
def obtener_event_up(request,painting_code):
    token = obtener_token() 
    resultado = realizar_peticion_get_event_up( painting_code, token)

    if 'error' in resultado:
        # Si hubo un error, renderizar la página de error
        return render(request, 'error.html', {'error': resultado['error']})
    else:
        # Si todo va bien, renderizar la tabla de luminaria
        print(resultado)
        return JsonResponse({'data': resultado['data']})